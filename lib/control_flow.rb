# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    str.delete!(char) if char == char.downcase
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  if str.length.odd?
    str[mid]
  else
    str[mid - 1..mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.chars.each do |char|
    count += 1 if VOWELS.include?(char)
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  (2..num).each { |i| product *= i }
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_str = ""
  arr.each_index do |i|
    new_str += arr[i].to_s
    new_str += separator unless i == arr.length - 1
  end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_str = ""
  str.chars.each_index do |i|
    if i.even?
      new_str += str[i].downcase
    else
      new_str += str[i].upcase
    end
  end
  new_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_words = []
  str.split.each do |word|
    if word.length >= 5
      word = word.reverse
    end
    new_words << word
  end
  new_words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  numbers = []
  (1..n).each do |i|
    if i % 3 == 0 && i % 5 == 0
      numbers << "fizzbuzz"
    elsif i % 3 == 0
      numbers << "fizz"
    elsif i % 5 == 0
      numbers << "buzz"
    else
      numbers << i
    end
  end
  numbers
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  arr.each { |el| new_arr.unshift(el) }
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2..num/2).each do |i|
    return false if num % i == 0
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |i|
    factors << i if num % i == 0
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors(num).each do |i|
    prime_factors << i if prime?(i)
  end
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |el|
    evens << el if el.even?
    odds << el if el.odd?
  end
  evens.length == 1 ? evens[0] : odds[0]
end
